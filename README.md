# certbot-generator

`certbot-generator` reads a list of domains and requests SSL certificates from
letsencrypt.org on their behalf using the `webroot` plugin. It takes an 
additional step of combining the the full chain and key into a single 
certificate file.

The image itself is designed to be run as a command that terminates and not a 
long running process.

**Important!** You need to run it in tandem with a web server that will give
letsencrypt.org access to validate the domain. This is explained a bit more
below.

## Usage

```
$ docker run -e CERT_EMAIL=you@email.com \
             -v challenges:/var/local/certbot/challenges \
             -v domain_list.txt:/var/local/certbot/in/domains \
             -v certs:/var/local/certbot/out \
             ceroic/certbot-generator
```

If successful certificates for each domain listed will be available in the 
output volume.

### Environment Variables

| Variable | Description |
| --- | --- |
| CERT_EMAIL | The email address used for certificate authentication. |

### Volumes

| Volume | Internal Path | Description |
| --- | --- | --- |
| Challenges | `/var/local/certbot/challenges` | This is where certbot will write the challenges to validate w/ letsencrypt. |
| Input | `/var/local/certbot/in` | Where to mount the input list file. (Should be mounted as a file named *domains* with no extension. |
| Output | `/var/local/certbot/out` | Where the combined certificates will be written after concatenation. |
| Let's Encrypt | `/etc/letsencrypt` | In production you should mount this to a persistent volume so certificates can be reused and renewed correctly. |

### Input List File

A simple file with one domain per line you would like a cert for. Should be mounted
to the Input volume with the filename `domains`. (There is no extension). 

Sample file contents:

```
mydomain1.com
mydomain2.com
yourdomain3.net
```

### Validation Server

Let's Encrypt has to be able to validate you have access to the domain before 
it will give you a certificate. To do this, they'll send a web request to the 
domain as explained here:

https://certbot.eff.org/docs/using.html#webroot

For the whole process to work you should run a simple webserver that serves
the mounted challenges volume. For example, here's a docker-compose snippet for
an nginx server:

```
certbot-server:
    image: nginx:stable
    volumes:
      - challenges:/usr/share/nginx/html
```

**challenges** in the above example would be the same **Challenges** volume 
mentioned under Volumes.

### Testing things locally

This process is difficult to test locally since Let's Encrypt needs to 
successfully send requests to your domain. Here's how we do it with the awesome
program **ngrok**. 

1. First start your certbot-server.
2. Start up **ngrok** and point it at your certbot-server.

```
$ ngrok http 192.168.99.100:80
```

This example assumes you are using docker-machine and have a separate IP.

3. Generate certs!

```
$ docker run -e CERT_EMAIL=you@email.com \
             -v challenges:/var/local/certbot/challenges \
             -v domain_list.txt:/var/local/certbot/in/domains \
             -v certs:/var/local/certbot/out \
             ceroic/certbot-generator
```

As mentioned above, in production you should also mount the lets encrypt directory
to a persistent volume so certs can be renewed / reused correctly.
