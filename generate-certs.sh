#!/usr/bin/env bash
set -e
while read d; do
    certbot certonly -q --keep --webroot --agree-tos -m ${CERT_EMAIL} -w /var/local/certbot/challenges -d $d
    cat /etc/letsencrypt/live/$d/fullchain.pem /etc/letsencrypt/live/$d/privkey.pem > /var/local/certbot/out/$d.pem
done </var/local/certbot/in/domains
